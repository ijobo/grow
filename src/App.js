import React, { Component } from 'react';
import { Route, BrowserRouter } from 'react-router-dom';
import Home from "./components/Home";
import LoginPage from "./components/Login";
import ReportDetails from "./Views/Reports/ReportDetails";
import { AppBar, Toolbar, Typography } from '@material-ui/core';

class App extends Component {
  constructor() {
    super();
    this.state = {
      isLogged: false
    }
  }
  render() {
    return (
      <BrowserRouter>
        <div>
          <AppBar position="static">
            <Toolbar variant="dense">
              <Typography variant="title" color="inherit">
                Grow
            </Typography>
            </Toolbar>
          </AppBar>
          <Route path='/' exact component={LoginPage} />
          <Route path='/Home' component={Home} />
          <Route path='/Reports' component={ReportDetails} />
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
