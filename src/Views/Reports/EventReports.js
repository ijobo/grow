import React, { Component } from 'react';
import { Table, TableHead, TableCell, TableRow, TableBody } from '@material-ui/core';

class EventReport extends Component {
    render() {
        return (
            <div>
                <Table style={{width:"800px"}}>
                    <TableHead>
                        <TableRow>
                            <TableCell>Report ID</TableCell>
                            <TableCell>Report Type</TableCell>
                            <TableCell>Report Date</TableCell>
                            <TableCell>Attachments Exist</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        <TableRow>
                            <TableCell>EventR001</TableCell>
                            <TableCell>Event Report</TableCell>
                            <TableCell>{new Date().toDateString()}</TableCell>
                            <TableCell>Y</TableCell>
                        </TableRow>
                    </TableBody>
                </Table>
            </div>
        )
    }
}

export default EventReport;