import React, { Component } from 'react';
import { Tab, Tabs, Typography } from '@material-ui/core';
import EventReport from "./EventReports";
import MonthlyReport from "./MonthlyReports";

function TabContainer(props) {
    return (
        <Typography component="div" style={{ padding: 8 * 3 }}>
            {props.children}
        </Typography>
    );
}


class ReportDetails extends Component {
    state = {
        value: 0,
    };

    handleChange = (event, value) => {
        this.setState({ value });
    };
    render() {
        const {value} = this.state;
        return (
            <div>
                <Tabs
                    value={value}
                    indicatorColor="primary"
                    textColor="primary"
                    onChange={this.handleChange}
                >
                    <Tab label="Event Reports" />
                    <Tab label="Monthly Reports" />
                </Tabs>

                {value === 0 && <TabContainer><EventReport/></TabContainer>}
                {value === 1 && <TabContainer><MonthlyReport/></TabContainer>}
            </div>
        )
    }
}


export default ReportDetails;