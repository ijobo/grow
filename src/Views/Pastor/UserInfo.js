import React, { Component } from "react";
import { CardContent, Typography, TextField, FormControlLabel,Checkbox, Grid } from '@material-ui/core';
import { Visibility, AccountCircle } from '@material-ui/icons';

class PastorUserInfo extends Component{
    render(){
        return(
          
                <CardContent>
                    <div style={{ display: "flex", flexDirection: "column", width: "300px" }}>
                        <div>
                            <Typography variant="headline" gutterBottom>User Information</Typography>
                        </div>
                        <div>
                            <Grid container spacing={8} alignItems="flex-end">
                                <Grid item>
                                    <AccountCircle color="primary"/>
                                </Grid>
                                <Grid item>
                                    <TextField id="input-with-icon-grid" label="Username" />
                                </Grid>
                            </Grid>
                        </div>
                        <div>
                            <Grid container spacing={8} alignItems="flex-end">
                                <Grid item>
                                    <Visibility color="primary"/>
                                </Grid>
                                <Grid item>
                                    <TextField id="input-with-icon-grid" type="password" label="Password" />
                                </Grid>
                            </Grid>
                        </div>
                        <div>
                            <Grid container spacing={8} alignItems="flex-end">
                                <Grid item>
                                    <Visibility color="primary"/>
                                </Grid>
                                <Grid item>
                                    <TextField id="input-with-icon-grid" type="password" label="Confirm Password" />
                                </Grid>
                            </Grid>
                        </div>
                       
                        <FormControlLabel
                            control={
                                <Checkbox
                                    value="checked"
                                    color="primary"
                                />
                            }
                            label="Use Default Password"
                        />
                    </div>
                </CardContent>
           
        )
    }
}

export default PastorUserInfo;