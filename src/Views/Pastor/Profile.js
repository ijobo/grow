import React, { Component } from 'react';
import { Card, CardContent, Typography, TextField, Grid, Select, MenuItem, InputLabel, FormControl, CardMedia, CardActions, Button } from '@material-ui/core';
import { AccountCircle, Person, Email, Call } from '@material-ui/icons';
import PlaceHolder from '../../images/pastor.jpg';


class PastorProfile extends Component {
    state = {
        gender: '',
        name: '',
    };

    handleChange = event => {
        this.setState({ [event.target.name]: event.target.value });
    };
    render() {
        return (
         
                <CardContent style={{ display: "flex" }}>
                    <div style={{ display: "flex", flexDirection: "column" }}>
                        <div>
                            <Typography variant="headline" gutterBottom>Pastor Information</Typography>
                        </div>
                        <div>
                            <Grid container spacing={8} alignItems="flex-end">
                                <Grid item>
                                    <Person color="primary" />
                                </Grid>
                                <Grid item>
                                    <TextField id="input-with-icon-grid" label="First Name" />
                                </Grid>
                                <Grid item>
                                    <TextField id="input-with-icon-grid" label="Last Name" />
                                </Grid>
                            </Grid>
                        </div>

                        <div>
                            <Grid container spacing={8} alignItems="flex-end">
                                <Grid item>
                                    <AccountCircle color="primary" />
                                </Grid>
                                <Grid item>
                                    <FormControl style={{ width: "180px" }}>
                                        <InputLabel htmlFor="gender">Gender</InputLabel>
                                        <Select
                                            value={this.state.gender}
                                            onChange={this.handleChange}
                                            inputProps={{
                                                name: 'gender',
                                                id: 'gender',
                                            }}
                                        >
                                            <MenuItem value={10}>Male</MenuItem>
                                            <MenuItem value={20}>Female</MenuItem>
                                            <MenuItem value={30}>Other</MenuItem>
                                        </Select>
                                    </FormControl>
                                </Grid>
                            </Grid>
                        </div>
                        <div>
                            <Grid container spacing={8} alignItems="flex-end">
                                <Grid item>
                                    <Email color="primary" />
                                </Grid>
                                <Grid item >
                                    <TextField style={{ width: "380px" }} id="input-with-icon-grid" label="E-Mail" />
                                </Grid>
                            </Grid>
                        </div>
                        <div>
                            <Grid container spacing={8} alignItems="flex-end">
                                <Grid item>
                                    <Call color="primary" />
                                </Grid>
                                <Grid item>
                                    <TextField id="input-with-icon-grid" label="Phone Number" />
                                </Grid>
                            </Grid>
                        </div>

                    </div>
                    <div>
                        <Card style={{
                            top: "50 %",
                            left: "50 %"}}>
                            <CardMedia style={{width:"100px", height:"100px", textAlign:"center"}}
                                image={PlaceHolder  }
                                title="Pastor Image" />
                            <CardActions>
                                <label htmlFor="contained-button-file">
                                    <Button variant="primary" component="span">
                                        Upload file
                                    </Button>
                                </label>
                            </CardActions>
                        </Card>
                    </div>
                </CardContent>
          
        )
    }
}

export default PastorProfile;