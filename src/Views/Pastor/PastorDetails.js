import React, { Component } from 'react';
import PastorAddress from './Address';
import PastorProfile from "./Profile";
import PastorUserInfo from "./UserInfo";
import SearchPastor from "./Search";
import { Button, Tab, Tabs, Typography, Card, CardContent } from '@material-ui/core';

function TabContainer(props) {
    return (
        <Typography component="div" style={{ padding: 8 * 3 }}>
            {props.children}
        </Typography>
    );
}


class PastorDetails extends Component {
    state = {
        value: 0,
    };

    handleChange = (event, value) => {
        this.setState({ value });
    };
    render() {
        const { value } = this.state;
        return (
            <div>
                <Tabs
                    value={value}
                    indicatorColor="primary"
                    textColor="primary"
                    onChange={this.handleChange}
                >
                    <Tab label="Search Pastor" />
                    <Tab label="Pastor Details" />
                </Tabs>

                {value === 0 && <TabContainer>
                    <SearchPastor />
                </TabContainer>}
                {value === 1 && <TabContainer>
                        <Card>
                            <CardContent>
                                <PastorProfile />
                                <PastorAddress />
                                <PastorUserInfo />
                                <Button variant="contained" color="primary">
                                    Save
                            </Button>
                            </CardContent>
                        </Card>
                </TabContainer>}
            </div>

        )


    }
}


export default PastorDetails;
