import React, { Component } from 'react';
import { CardContent, Typography, TextField, Grid } from '@material-ui/core';
import { Business, LocationCity, Place } from '@material-ui/icons';

class PastorAddress extends Component {
    render() {
        return (
            <CardContent>
                <div style={{ display: "flex", flexDirection: "column" }}>
                    <div>
                        <Typography variant="headline" gutterBottom>Address Information</Typography>
                    </div>
                    <div style={{ display: "flex" }}>
                        <Grid container spacing={8} alignItems="flex-end">
                            <Grid item>
                                <Business color="primary" />
                            </Grid>
                            <Grid item>
                                <TextField id="input-with-icon-grid" label="Line One" />
                            </Grid>
                            <Grid item>
                                <TextField
                                    id="input-with-icon-textfield"
                                    label="Line Two"
                                />
                            </Grid>
                        </Grid>
                    </div>
                    <div style={{ display: "flex" }}>
                        <Grid container spacing={8} alignItems="flex-end">
                            <Grid item>
                                <LocationCity color="primary" />
                            </Grid>
                            <Grid item>
                                <TextField id="input-with-icon-grid" label="City" />

                            </Grid>
                            <Grid item>
                                <TextField
                                    id="input-with-icon-textfield"
                                    label="State"
                                />
                            </Grid>
                        </Grid>
                    </div>
                    <div style={{ display: "flex" }}>
                        <Grid container spacing={8} alignItems="flex-end">
                            <Grid item>
                                <Place color="primary" />
                            </Grid>
                            <Grid item>
                                <TextField id="input-with-icon-grid" label="PinCode" />

                            </Grid>
                            <Grid item>
                                <TextField
                                    id="input-with-icon-textfield"
                                    label="Country"
                                />
                            </Grid>
                        </Grid>
                    </div>
                </div>
            </CardContent>
        )
    }
}

export default PastorAddress;