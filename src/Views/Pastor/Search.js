import React, { Component } from 'react';
import { Card, CardContent, TextField, Grid, Button, Table, TableHead, TableBody, TableCell, TableRow } from '@material-ui/core';
import { Person, Call, Business } from '@material-ui/icons';
import { Link } from "react-router-dom";

var styles = {
    buttonMargin: {
        margin: "15px"
    },
    titlePadding: {
        padding: "10px"
    },
    tableWidth:{
        width:"600px"
    }
}

class SearchPastor extends Component {
    render() {
        return (
            <Card>
              
                <CardContent>
                    <div style={{ display: "flex", flexDirection: "column", width:"300px" }}>
                        <div style={{ display: "flex" }}>
                            <Grid container spacing={8} alignItems="flex-end">
                                <Grid item>
                                    <Person color="primary" />
                                </Grid>
                                <Grid item>
                                    <TextField id="input-with-icon-grid" label="First Name" />
                                </Grid>
                            </Grid>
                        </div>
                        <div style={{ display: "flex" }}>
                            <Grid container spacing={8} alignItems="flex-end">
                                <Grid item>
                                    <Person color="primary" />
                                </Grid>
                                <Grid item>
                                    <TextField id="input-with-icon-grid" label="Last Name" />
                                </Grid>
                            </Grid>
                        </div>
                        <div style={{ display: "flex" }}>
                            <Grid container spacing={8} alignItems="flex-end">
                                <Grid item>
                                    <Call color="primary" />
                                </Grid>
                                <Grid item>
                                    <TextField id="input-with-icon-grid" label="Phone Number" />
                                </Grid>
                            </Grid>
                        </div>
                        <div style={{ display: "flex" }}>
                            <Grid container spacing={8} alignItems="flex-end">
                                <Grid item>
                                    <Business color="primary" />
                                </Grid>
                                <Grid item>
                                    <TextField id="input-with-icon-grid" label="City" />
                                </Grid>
                            </Grid>
                        </div>
                        <div>
                            <Button style={styles.buttonMargin} variant="contained" color="primary" aria-label="Search">
                                Search
                            </Button>
                            <Button className="buttonMargin" variant="contained" color="primary" aria-label="Clear">
                                Clear
                            </Button>
                        </div>
                    </div>
                    <div style={styles.tableWidth}>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>Pastor Name</TableCell>
                                    <TableCell>Phone Number</TableCell>
                                    <TableCell>City</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                <TableRow>
                                    <TableCell> <Link to="/Reports">John Doe</Link></TableCell>
                                    <TableCell>9898989898</TableCell>
                                    <TableCell>Hyderabad</TableCell>
                                </TableRow>
                            </TableBody>
                        </Table>
                    </div>
                </CardContent>
            </Card>
        )
    }
}

export default SearchPastor;