import React, { Component } from 'react';
import { TextField, Button, CardContent, Typography, Card } from '@material-ui/core';

var styles = {
    buttonMargin: {
        margin: "10px"
    }
}

class LoginPage extends Component {

    constructor() {
        super();
        this.state = {
            username: "",
            password: ""
        }
    }

    handleSubmit = () => {

        const path = '/Home';
        this.props.history.push(path);

    }

    render() {
        return (
            <form id="loginForm" onSubmit={this.handleSubmit}>
                <div className="center_div">
                    <Card>
                        <CardContent>
                            <div>
                                <Typography variant="headline" gutterBottom>Login Page</Typography>
                            </div>
                            <TextField className="txtfield"
                                label="Username"
                                id="username"
                                value={this.state.username}
                                onChange={e => this.setState({ username: e.target.value })} />
                            <TextField className="txtfield"
                                label="Password"
                                id="password"
                                value={this.state.password}
                                onChange={e => this.setState({ password: e.target.value })}
                                type="password" />
                            <Button style={styles.buttonMargin} type="submit">Login</Button>
                            <Button style={styles.buttonMargin}>Forgot Password</Button>
                        </CardContent>
                    </Card>
                </div>
            </form>
        )
    }
}

export default LoginPage